# Instructor's Guide

My courses evolve over time; hopefully for the better.
So I suggest looking at the most recent versions of this course
here:

https://gitlab.com/wne-csit-jackson/cs351/

## Schedule

My schedule usually lives in a live Google Doc or Google Sheet
and update it as I teach. So here is my schedule at the
end of Fall 2024.

![](./Schedule-2024-Fall.png)

## Readings

When I first started teaching with PLCC,
I assigned readings from Tim's [JNotes](../JNotes/).
Over time, I've been revising and reformatting Tim's notes
into some Markdown files. For the first two weeks of my course,
students read these Markdown files. Students switch to Tim's
notes (in PDF format) around call semantics. This may change
in the future as I continue revising and reformating material.

## GitLab and GitPod

In any course in which I expect students to code, I use GitLab and GitPod
as an LMS. Specifically, I use them to disseminate materials, homework
submission, and to provide a consistent development environment pre-installed
with PLCC. What follows is a description of how I use these tools in my
courses. I do not provide feedback and grades in this environment. For
grading, I use my school's designated LMS.

First, I create a GitLab group for each course I offer.

In that group, I create a project that will hold my course materials.
I populate this project over time based on my previous offering of the
course. As I do, I'm reviewing, revising, and reorganizing course materials
based on my experiences in the previous offering and my evolving view of
software development and pedagogical technologies and practices.

In the course group, I also create an `Individual` group.
In this `Individual` group, I create a private project for each student
and give each student read and write privileges (maintainer) over their
project. These projects contain the same material as the course project
I described in the previous paragraph.

When I assign an assignment, I add starter files for the assignment to
the course project and then to each student's individual projects.

Students open their individual projects in GitPod, do their work,
commit and push it back to their individual projects.

The magic is in a `.gitpod.yml` file. Each project (including the course
project) has one of these files. This file is configured to configure
GitPod with the tools and configuration  install
PLCC into a GitPod workspace and provide any other configuration I want
my students to have while working with the course materials.

To grade, I open each student's individual project in GitPod, review
their work, and provide feedback in the school's LMS.
